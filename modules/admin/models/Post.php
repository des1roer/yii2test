<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property string $body
 * @property integer $user_id
 * @property string $username
 * @property string $f_timestamp
 *
 * @property User $user
 */
class Post extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['body', 'user_id', 'username'], 'required'],
            [['body', 'username'], 'string'],
            [['user_id'], 'integer'],
            [['f_timestamp'], 'safe'],
                //[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'body' => '????? ?????????',
            'user_id' => 'id ???????????',
            'username' => '??? ???????????',
            'f_timestamp' => '??????????',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function initPost()
    {
        return static::find()
                        ->orderBy([
                            'f_timestamp' => SORT_DESC,
                        ])
                        ->limit(10)
                        ->all();
    }

    /**
     * @inheritdoc
     * @return PostQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PostQuery(get_called_class());
    }

}
