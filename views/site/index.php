<style>
    .form-horizontal .control-group {
        margin-bottom: 20px;
    }


    .alert, .alert h4 {
        color: #c09853;
    }
    .alert {
        background-color: #fcf8e3;
        border: 1px solid #fbeed5;
        border-radius: 4px;
        margin-bottom: 20px;
        padding: 8px 35px 8px 14px;
        text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
    }
    .alert-danger, .alert-error {
        background-color: #f2dede;
        border-color: #eed3d7;
        color: #b94a48;
    }
    textarea, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
        background-color: #fff;
        border: 1px solid #ccc;
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
        transition: border 0.2s linear 0s, box-shadow 0.2s linear 0s;
        border-radius: 4px;
    }
</style>
<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

//use app\modules\admin\models\Post;
?>
<div class="row-fluid">
    <div class="span2"></div>
    <div class="span8">

        <div id="alert" class="alert alert-error">
            Сообщение не может быть пустым
        </div>
<?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

        <div class="control-group">


            <?=
            \yii\helpers\Html::textarea('textarea', null, $options = ['id' => 'textarea',
                'placeholder' => "Ваше сообщение...",
                'style' => "width: 100%; height: 50px; margin-bottom: 20px;",
                'disabled' => Yii::$app->user->isGuest])
            ?>
        </div>     
        <div class="control-group">
            <?=
            Html::submitButton('Отправить сообщение', ['id' => 'submit_', 'class' => 'btn btn-primary', 'name' => 'contact-button',
                'style' => "margin-bottom: 20px;", 'disabled' => 'true'])
            ?>
        </div>

<?php ActiveForm::end(); ?>


        <script>

            var textarea = document.getElementById('textarea');
            var alert = document.getElementById('alert');
            var submit = document.getElementById('submit_');

            textarea.onkeyup = function () {
                if (this.value.match(/^[ ]+$/)) { // В значении только пробелы
                    this.value = ''
                }
                if (this.value !== '') {
                    alert.style.display = 'none';
                    submit.disabled = false;
                }
                else
                if (this.value === '') {
                    alert.style.display = 'block';
                    submit.disabled = true;
                }
            };
        </script>
        <?php
        $arr = app\modules\admin\models\Post::initPost();
        for ($i = 0, $cnt = count($arr); $i < $cnt; $i++)
        {
            ?>
            <div class="well"><h5><b>
                        <?php
                        echo $arr[$i]->username;
                        ?>:</b></h5>
                <?php
                echo $arr[$i]->body;
                ?>
            </div>
            <?php
        }
        ?>  


    </div>
</div>


</body>
</html>